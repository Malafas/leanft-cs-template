﻿using HP.LFT.Verifications;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

//using Aspose.Cells;



namespace TemplateLeanFtCS.FileReader
{
    public class Util
    {
/*        //non-inverted = header on first line
        //inverted = header on first column
        private static object[,] ReadExcelData(string fileName)
        {
            string licencePath = ConfigurationManager.AppSettings["ASPOSE_LICENCE_PATH"]; //"./Aspose.total.lic";
            Aspose.Cells.License license = new Aspose.Cells.License();
            license.SetLicense(Path.GetFullPath(licencePath));
            //Creating a file stream containing the Excel file to be opened
            FileStream fstream = new FileStream(fileName, FileMode.Open, FileAccess.Read);

            //Instantiating a Workbook object
            //Opening the Excel file through the file stream
            Workbook workbook = new Workbook(fstream);

            //Accessing the first worksheet in the Excel file
            Worksheet worksheet = workbook.Worksheets[0];

            //string a = worksheet.Cells.ExportArray();
            var row = worksheet.Cells.MaxDisplayRange.RowCount;
            var col = worksheet.Cells.MaxDisplayRange.ColumnCount;
            //Exporting the contents of 7 rows and 2 columns starting from 1st cell to DataTable
            return worksheet.Cells.ExportArray(0, 0, row, col);
        }

        private static void Debug(string v)
        {
            throw new NotImplementedException();
        }


        //return index column of the excel matrix of testId
        private static int GetColumnTestId(Object[,] matrix, string testId, string columnId)
        {
            // Constant.CIENT_ID = clé de la ligne des identifiants uniques
            int indexLineClient = GetLineKey(matrix, columnId);
            //GetUpperBound(1) = total columns
            for (int columnClient = 1; columnClient <= matrix.GetUpperBound(1); columnClient++)
            {
                if (matrix[indexLineClient, columnClient].ToString().Equals(testId))
                {
                    return columnClient;
                }
            }
            throw new Exception("No testKey '" + testId + "' corresponding in excel file");
        }

        //return index row of the excel matrix of keyName
        private static int GetLineKey(Object[,] matrix, string keyName)
        {
            int indexColumnHeader = 0;
            //GetUpperBound(0) = total rows
            for (int line = 0; line <= matrix.GetUpperBound(0); line++)
            {
                if (matrix[line, indexColumnHeader].Equals(keyName))
                {
                    return line;
                }
            }
            return 0;
        }

        private static Dictionary<string, string> GetTestDataFromMatrix(Object[,] matrix, int valueColumn, int keyColumn = 0)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            for (int line = 0; line < matrix.GetUpperBound(0); line++)
            {
                string value = null;
                string key = matrix[line, keyColumn].ToString();
                if (matrix[line, valueColumn] != null)
                {
                    value = matrix[line, valueColumn].ToString();
                }
                result[key] = value;
            }
            return result;
        }

        public static Dictionary<string, string> GetData(String idClient, String columnId, String dataFileName)
        {
            object[,] matrix = Util.ReadExcelData(dataFileName);
            // 0 est la colonne où il y a les clés/attributs
            int paramsColumn = 0; // numéro de la colonne où se situe le nom des données
            int focusClientColumn = Util.GetColumnTestId(matrix, idClient, columnId);
            return Util.GetTestDataFromMatrix(matrix, focusClientColumn, paramsColumn);
            //accéder à la valeur "nom" de l'excel pour la colonne à tester : hashmap["nom"]
        }
*/    }
}

