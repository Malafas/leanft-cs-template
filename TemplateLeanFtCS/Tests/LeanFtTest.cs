﻿using System;
using NUnit.Framework;
using HP.LFT.SDK;
using HP.LFT.Verifications;
using HP.LFT.SDK.Web;
using FormSelenium;

namespace TemplateLeanFtCS
{
    
    [TestFixture]
    public class LeanFtTest : UnitTestClassBase
    {
        protected IBrowser browser;

        [OneTimeSetUp]
        public void TestFixtureSetUp()
        {
            // Setup once per fixture
        }

        //Execute code before each test
        [SetUp]
        public void SetUp()
        {
            //Open the browser
            browser = BrowserFactory.Launch(BrowserType.Chrome); 
            //browser = BrowserFactory.Launch(BrowserType.Firefox);
            //browser = BrowserFactory.Launch(BrowserType.InternetExplorer);
        }

        [Test]
        public void Test()
        {
            //Navigate to the url and wait the download
            browser.Navigate("http://www.softpost.org/selenium-test-page/");
            browser.Sync();

            //Search the model FormSeleniumPage
            FormSeleniumPage appModel = new FormSeleniumPage(browser);

            //selection and filling fields
            appModel.seleniumTestPage.firstName.SetValue("Hello");
            appModel.seleniumTestPage.lastName.SetValue("World");
            appModel.seleniumTestPage.mobile.SetValue("0000000000");
            appModel.seleniumTestPage.mail.SetValue("hello.world@gmail.com");
            appModel.seleniumTestPage.selectCity.Select(1); //select Mumbai
            appModel.seleniumTestPage.genderRadioGroup.Select(0);
            appModel.seleniumTestPage.qtpCheckBox.Click();
            appModel.seleniumTestPage.seleniumCheckBox.Click();
            appModel.seleniumTestPage.signUpButton.Click();
        }

        //Execute code after each test
        [TearDown]
        public void TearDown()
        {
            //Close the browser
            //browser.Close();
        }

        [OneTimeTearDown]
        public void TestFixtureTearDown()
        {
            // Clean up once per fixture
        }
    }
}
